
import { _ } from 'svelte-i18n';
import { get } from 'svelte/store'

import { SIDEBAR_TRANSLATIONS } from '$lib/Translations/SidebarTranslations';
import { add_translation } from '@perinet/libperiwebui';

add_translation("de", SIDEBAR_TRANSLATIONS.de);
add_translation("en", SIDEBAR_TRANSLATIONS.en);

export const SidebarMenu = [
    {
        name: "",       // headline for a folding menu
        folding: false, // is it a flat or a folding menu
        targets: [
            {
                link: "/staticfiles/",
                label: get(_)('sidebar.label.home')
            },
            {
                link: "/staticfiles/info",
                label: get(_)('sidebar.label.info')
            },
            {
                link: "/staticfiles/status",
                label: get(_)('sidebar.label.status')
            }
        ]
    },
    {
        name: "",       // headline for a folding menu
        folding: false, // is it a flat or a folding menu
        targets: [
            {
                link: "/staticfiles/node_config", label:
                get(_)('sidebar.label.node_config')
            },
            {
                link: "/staticfiles/security_config",
                label: get(_)('sidebar.label.security_config')
            },
            {
                link: "/staticfiles/firmware_update",
                label: get(_)('sidebar.label.firmware_update')
            },
        ]
    },
    {
        name: "Documentation",  // headline for a folding menu
        folding: false,          // is it a flat or a folding menu
        targets: [
            {
                link: "/staticfiles/api_documentation",
                label: get(_)('sidebar.label.api_documentation')
            },
            {
                link: "https://docs.perinet.io",
                label: get(_)('sidebar.label.online_documentation')
            },
        ]
    },
];
