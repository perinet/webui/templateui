export const SIDEBAR_TRANSLATIONS = {
    en: {
        "sidebar.label.home": "Home",
        "sidebar.label.info": "Information",
        "sidebar.label.status": "Status",
        "sidebar.label.node_config": "Node config",
        "sidebar.label.security_config": "Security config",
        "sidebar.label.firmware_update": "Firmware_update",
        "sidebar.label.api_documentation": "API documentation",
        "sidebar.label.online_documentation": "Online documentation"
    },
    de: {
    }
};
